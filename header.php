<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Museum_Style
 */

?>




<!DOCTYPE html>
<html>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<script>
function goBack() {
    window.history.back();
}
</script>

<?php wp_head(); ?>
</head

<body class = "esp" <?php body_class(); ?>>

<div id="page" class="site">
	<div id="content" class="site-content">

 	<script type="text/javascript">
	 	   
	 	if(localStorage.cName){
	 		 document.body.className = localStorage.getItem("cName");
	    }else{
	    	localStorage.setItem("cName", "esp");
	    // Retrieve
	    document.body.className = localStorage.getItem("cName");
	    }
 	</script>

<header>
	<nav role="navigation">
		<div class="navbar navbar-fixed-top navbar-inverse bg-inverse no-click" id="nv_hdn" data-spy="affix" data-offset-top="900">
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" id="t_hdn" data-spy="affix" data-offset-top="900" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo( 'name' ) ?>" rel="homepage"><?php bloginfo( 'name' ) ?></a>
					<button type="button" class="btn navbar-btn pull-right" id="topbar" 
					data-toggle="collapse" data-target="#menu"><i class="material-icons" id="m_i">menu</i>
					</button>
				</div>
				<div id="menu" class="panel panel-default panel-collapse collapse">
					<div class="container">
				    		
				    			<h4 lang="esp"><a href ='<?php the_permalink(33) ?>'>Inicio</a></h4>
				    			<h4 lang="esp"><a href ='<?php the_permalink(225) ?>'>¿Quiénes somos?</a></h4>
				    			<h4 lang="esp"><a href ='<?php the_permalink(237) ?>'>Acervos</a></h4>
				    			<h4 lang="esp"><a href ='<?php the_permalink(235) ?>'>Ruta de Plata</a></h4>

				    			<h4 lang="esp"><a href="#hue" data-toggle="collapse">Museos</a></h4>
									<div id="hue" class="collapse menu_museos">
										<div class="container">
												<?php wp_nav_menu( array( "theme_location" => 'footer-menu-2') ) ?>
										</div>
									</div>
								<h4 lang="esp"><a href ='<?php the_permalink(228) ?>'>Servicios</a></h4>
				    			<h4 lang="esp"><a href ='<?php the_permalink(35) ?>'>Artículos</a></h4>
				    			<h4 lang="esp"><a href ='<?php the_permalink(37) ?>'>Eventos</a></h4>
				    			<h4 lang="esp"><a href ='<?php the_permalink(231) ?>'>Contacto</a></h4>
				    			<h4 lang="esp"><a href ='<?php the_permalink(277) ?>'>Tienda</a></h4>

				    		<!--English-->
				    			<h4 lang="en"><a href ='<?php the_permalink(33) ?>'>Home</a></h4>
				    			<h4 lang="en"><a href ='<?php the_permalink(225) ?>'>About us</a></h4>
				    			<h4 lang="en"><a href ='<?php the_permalink(237) ?>'>Acervos</a></h4>
				    			<h4 lang="en"><a href ='<?php the_permalink(235) ?>'>Ruta de Plata</a></h4>

				    			<h4 lang="en"><a href="#hue2" data-toggle="collapse">Museums</a></h4>
									<div id="hue2" class="collapse menu_museos">
										<div class="container">
												<?php wp_nav_menu( array( "theme_location" => 'footer-menu-2') ) ?>
										</div>
									</div>

								<h4 lang="en"><a href ='<?php the_permalink(228) ?>'>Services</a></h4>
				    			<h4 lang="en"><a href ='<?php the_permalink(35) ?>'>Articles</a></h4>
				    			<h4 lang="en"><a href ='<?php the_permalink(37) ?>'>Events</a></h4>
				    			<h4 lang="en"><a href ='<?php the_permalink(231) ?>'>Contact</a></h4>
				    			<h4 lang="en"><a href ='<?php the_permalink(277) ?>'>Shop</a></h4>

				  	</div>
				</div>
			</div>
		</div>           
	</nav>
</header>


<?php 	$img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-tumbnail');
		$subtitle = get_post_custom_values($sub = 'subtitulo');?>

<?php if (is_front_page()):?>

	<div class="back_img" id="front_h" style="background-image: url('<?php echo $img[0]?>');">
		<div class = "opa" id="fp_opa"> 
			<div id="h_position_fp">
			<div class="container">
				<?php the_custom_logo();?> 
				<div class="title_fp"><h1><?php the_title();?></h1> 
					<p><?php echo $subtitle[0]; ?></p>
				</div>
			</div>
			</div>
		</div>
	</div>	

<?php elseif (is_page()):?>
	<div class="back_img" id="pages_h" style="background-image: url('<?php echo $img[0]?>');">
		<div class = "opa" id="dif_opa"> 
			<div id="h_position_pages">
				<h1> <?php the_title();?></h1> 
				<p><?php echo $subtitle[0];?></p>
			</div>
		</div>
	</div>


	<?php 	
		$location = get_post_custom_values($key ='location');
		$HrSemana = get_post_custom_values($hr = 'HrSemana');
		$mapa = get_post_custom_values($map = 'mapa');

	if($location):?>
		<a id="scrollerButton" href="#demo" data-toggle="collapse">
			<div id="scrollerTop" class="btn-location valign-wrapper">
				<div class="container baskerville">
					<div class="row">
						<div class="col-sm-7 align-left location-text valign-wrapper">
							<i class='material-icons location-icons'>location_on</i>
							<div>
								<?php echo $location[0];?>
							</div>
						</div>
						<div class="col-sm-5 align-left location-text valign-wrapper small-padding-left">
							<div>
								Horarios de atención
							</div>
							<i class='material-icons location-icons'>arrow_drop_down</i>
						</div>
					</div>
				</div>
			</div>
		</a>
		<div id="demo" class="collapse info-location">
			<div class="container location-container">
				<div class="row">
					<div class="col-sm-7">
						<div class="map-wrapper">
							<?php echo $mapa[0];?>
						</div>
					</div>
					<div class="col-sm-5 timetable-text">
						<?php echo get_field('timetable'); ?>
					</div>
				</div>
			</div>
		</div>

		<script type="text/javascript">
		(function($) {
			$("#scrollerButton").click(function() {
				$('html, body').animate({
					scrollTop: $("#scrollerTop").offset().top
				}, 500);
			});
		})( jQuery );
		</script>

	<?php endif;?>



<?php elseif (is_single()):


		$location = get_post_custom_values($key ='location');
		$HrSemana = get_post_custom_values($hr = 'HrSemana');
		$mapa = get_post_custom_values($map = 'mapa');

	if($location):?>


	<div class="back_img" id="pages_h" style="background-image: url('<?php echo $img[0]?>');">
		<div class = "opa" id="dif_opa"> 
			<div id="h_position_pages">
				<h1> <?php the_title();?></h1> 
				<p><?php echo $subtitle[0];?></p>
			</div>
		</div>
	</div>



		<a id="scrollerButton" href="#demo" data-toggle="collapse">
			<div id="scrollerTop" class="btn-location valign-wrapper">
				<div class="container baskerville">
					<div class="row">
						<div class="col-sm-7 align-left location-text valign-wrapper">
							<i class='material-icons location-icons'>location_on</i>
							<div>
								<?php echo $location[0];?>
							</div>
						</div>
						<div class="col-sm-5 align-left location-text valign-wrapper small-padding-left">
							<div>
								Horarios de atención
							</div>
							<i class='material-icons location-icons'>arrow_drop_down</i>
						</div>
					</div>
				</div>
			</div>
		</a>
		<div id="demo" class="collapse info-location">
			<div class="container location-container">
				<div class="row">
					<div class="col-sm-7">
						<div class="map-wrapper">
							<?php echo $mapa[0];?>
						</div>
					</div>
					<div class="col-sm-5 timetable-text">
						<?php echo get_field('timetable');?>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
		(function($) {
			$("#scrollerButton").click(function() {
				$('html, body').animate({
					scrollTop: $("#scrollerTop").offset().top
				}, 500);
			});
		})( jQuery );
		</script>


	<?php else:?>
	<div class="back_img" id="post_h" style="background-image: url('<?php echo $img[0]?>');">
		<div class = "opa" id="dif_opa"> 
			<div id="h_position_pages">
				<h1> <?php the_title();?></h1> 
			</div>
		</div>
	</div>	



	<?php endif;?>



<?php endif;?>
