<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Museum_Style
 */

get_header(); ?>

<div class="errorsectioan">
	<div class="col-sm-3 col-xs-3 col-md-3 col-lg-3"></div>
	<div class="col-sm-6 col-xs-6 col-md-6 col-lg-6">
			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="errortext page-title">Error 404 - Recurso no encontrado</h1>
				</header><!-- .page-header -->
				<div class="btn404"> 
					<button type="button" class="btn btn-default return" onclick="goBack()">
						<span class="glyphicon glyphicon-arrow-left"></span> Regresar
					</button><!-- .page-content -->
				</div>
			</section><!-- .error-404 -->
	</div>
	<div class="col-sm-3 col-xs-3 col-md-3 col-lg-3"></div>
</div>


<?php
get_footer();?>