<?php get_header() ;?>

<div class="container" id="margin_PC">
	<div class="card-colums">
		<?php query_posts('cat=11'); while (have_posts()) : the_post(); ?>
			<div class="col-sm-6 col-md-4 col-lg-4">
				<div class="card card-hover">
					<?php if (has_post_thumbnail()):?>
						<a href="<?php the_permalink();?>"><img class="card-img-top"  id = "img-res" src= "<?php the_post_thumbnail_url();?>" alt="<?php the_title()?>"></a>
					<?php else:?>
						<a href=  "<?php the_permalink() ;?>" ><img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20%282%29.jpg" alt="<?php the_title()?>"></a>
					<?php endif;?>						
						<div class="card-block" id="h_c">
							<a href=  "<?php the_permalink() ;?>"><h2 class="text_card-title"><?php the_title(); ?></h2></a>
							<p class="card-text" id="text_card_hdn"><?php echo excerpt(20) ?></p>
							<a href=  "<?php the_permalink() ;?>" class="pull-right"> Ver más </a>
						</div>
				</div>
			</div>
		<?php endwhile; 
			wp_reset_query(); ?>
	</div>
</div>

<?php get_footer() ; ?>