<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Museum_Style
 */

?>

	</div><!-- #content -->

	
	<footer id="colophon" class="site-footer" role="contentinfo">
	<div>
	</div>
		<div class="container">
			<div class="row icons">
				<div class="col-md-3 col-lg-3"></div>
				<div class="col-md-6 col-lg-6">
					<ul class="social-icons icon-circle icon-zoom list-unstyled list-inline">
						<li><a href="http://www.google.com" class="icons-seis"><img class="circular-img" src="<?php echo get_bloginfo('template_url')?>/img/doge.png"/></a></li>
						<li><a href="http://www.google.com" class="icons-seis"><img class="circular-img" src="<?php echo get_bloginfo('template_url')?>/img/doge.png"/></a></li>
						<li><a href="http://www.google.com" class="icons-seis"><img class="circular-img" src="<?php echo get_bloginfo('template_url')?>/img/doge.png"/></a></li>
						<li><a href="http://www.google.com" class="icons-seis"><img class="circular-img" src="<?php echo get_bloginfo('template_url')?>/img/doge.png"/></a></li>
						<li><a href="http://www.google.com" class="icons-seis"><img class="circular-img" src="<?php echo get_bloginfo('template_url')?>/img/doge.png"/></a></li>
						<li><a href="http://www.google.com" class="icons-seis"><img class="circular-img" src="<?php echo get_bloginfo('template_url')?>/img/doge.png"/></a></li>
					</ul>
				</div>
				<div class="col-md-3 col-lg-3"></div>
			</div>
			<div class="row footer-menu">
				<div class="col-md-3 col-lg-3 logo-footer">
					<?php the_custom_logo();?>
				</div>
				<div class="col-md-2 col-lg-2 ">
					<div id="menu1" lang="esp">
						<br>
						<?php wp_nav_menu( array( "theme_location" => 'footer-menu-1' ) ) ?>
					</div>
					<div id="menu1" lang="en">
						<br>
						<?php wp_nav_menu( array( "theme_location" => 'footer-menu-1-en' ) ) ?>
					</div>
				</div>
				<div class="col-md-3 col-lg-3">
					<nav id="menu2">
						<h4 lang="esp">Museos</h4><h4 lang="en">Museums</h4>
						<?php wp_nav_menu( array( "theme_location" => 'footer-menu-2') ) ?>
					</nav>
				</div>
				<div class="col-md-3 col-lg-3">
					<h4 id= "menu2" lang="esp"><a href ='<?php the_permalink(231) ?>'>Contacto</a></h4>
					<h4 id= "menu2" lang="en"><a href ='<?php the_permalink(231) ?>'>Contact</a></h4>
					<p>Calle de Mina No. 110, Col. Centro Pachuca de Soto, Hidalgo <br> T. +52 1 (771) 7150976 <br> ahmm.museos@gmail.com</p>
				</div>
				<div class="col-md-1 col-lg-1">
					<ul class="social-icons icon-circle icon-zoom list-unstyled list-inline">
						<li> <a href="https://www.facebook.com/ArchivoHistoricoyMuseodeMineriaAC/"><i class="fa fa-facebook"></i></a></li>  
					</ul>
					<div class="idiomas">
						<a class="changeEn" style="cursor:pointer">English</a>	
						<a class="changeEsp" style="cursor:pointer">Español</a>
					</div>
				</div>
			</div>
		<div>
		
	</footer><!-- #colophon -->
	<div class="copyFooter">
			<p >&copy;<?php echo date('Y'); ?></p>	
		</div>	
</div><!-- #page -->

<?php wp_footer(); ?>

</body>


<script type="text/javascript">
(function($) {
	var click = 0;
	$("#topbar").click(function(){
		if(click == 0){
			$('#topbar i').text("clear");
			click++;
			
		}
		else{
			$('#topbar i').text("menu");
			click--;
		}
	});
})( jQuery );
</script>
<script type="text/javascript">
(function($) {

		$(".changeEsp").click(function(){
	    	localStorage.cName = "esp";
	    	console.log(localStorage.cName);
 			document.body.className = localStorage.getItem("cName");
			});

		$(".changeEn").click(function(){
	    	localStorage.cName = "en";
			document.body.className = localStorage.getItem("cName");
		});
})( jQuery );
</script>

</html>
	