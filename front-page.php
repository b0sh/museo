<?php
get_header() ;?>


<div class="container">
	<h1 id="titulo_fp_AE" lang="esp">Nuestros museos</h1>
	<h1 id="titulo_fp_AE" lang="en">Museums</h1>

	<div class="row">
		<div class="card-group">
			<?php
				$pages = array(5,7,9,11);
				foreach ($pages as $id ) {	
					$post = get_post($id);
					$title = $post->post_title;
					$summary = $post->post_excerpt;
					$link = get_the_permalink($id);
					$location = get_post_custom_values($loc = "location");

					
					echo "<div class='col-sm-12 col-md-6 col-lg-6'><div class='card card-hover' >";

					if (has_post_thumbnail($post)){
						$img = wp_get_attachment_image_src(get_post_thumbnail_id($post), 'single-post-tumbnail');
						echo '<a href="'. $link .'"><img class="card-img-top"  id = "img-res" src= "' . $img[0] . '" alt="'.$title.'"></a>';
					}
					else{
						echo '<img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20%282%29.jpg" alt="'.$title.'">';
					}

					echo "<div class='card-block' id='h_card_fp'><h2 class='text_card-title'><a href=". $link .">" . $title . "</a></h2>";
					echo "<p class='card-text' id='h_card_text_fp'>" . $summary ."</p><br>";

					echo "<p><i class='material-icons' id='v_al'>location_on</i>" . " " .$location[0] . "</p>";

					echo "<a href=" . $link ." class='pull-right' > Ver más </a>";
					echo '</div></div></div>';
				}
			?>
		</div>
	</div>


	<h1 id="titulo_fp_AE" lang="esp"><a href="<?php the_permalink(37)?>">Eventos</a></h1>
	<h1 id="titulo_fp_AE" lang="en"><a href="<?php the_permalink(37)?>">Events</a></h1>
	
	<div class="card-deck">
		<?php query_posts('cat=11&showposts=3'); while (have_posts()) : the_post(); ?>
				<div class="col-sm-6 col-md-4 col-lg-4">
					<div class="card card-hover">
						<?php if (has_post_thumbnail()):?>
							<a href="<?php the_permalink();?>"><img class="card-img-top"  id = "img-res" src= "<?php the_post_thumbnail_url();?>" alt="<?php the_title()?>"></a>
						<?php else:?>
							<a href=  "<?php the_permalink() ;?>" ><img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20%282%29.jpg" alt="<?php the_title()?>"></a>
						<?php endif;?>						
							<div class="card-block" id="h_c">
								<a href=  "<?php the_permalink() ;?>"><h2 class="text_card-title"><?php the_title(); ?></h2></a>
								<p class="card-text" id="text_card_hdn"><?php echo excerpt(20) ?></p>
								<a href=  "<?php the_permalink() ;?>" class="pull-right"> Ver más </a>
							</div>
					</div>
				</div>
		<?php endwhile; wp_reset_query(); ?>
	</div>


</div>

</div> <!--page -->

</div><!--content -->

<?php
get_footer() ;
?>
